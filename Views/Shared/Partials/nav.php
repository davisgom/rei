<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="sr-only">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill site-nav" role="navigation">
            <li>
              <a href="home">
                home
              </a>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                about
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item <?php if ($page_content == "about") {echo "active";} ?>" href="about">overview</a>
                  <a class="dropdown-item <?php if ($page_content == "mission") {echo "active";} ?>" href="mission">mission</a>
                  <a class="dropdown-item <?php if ($page_content == "orgchart") {echo "active";} ?>" href="orgchart">organizational chart</a>
                  <a class="dropdown-item <?php if ($page_content == "staff") {echo "active";} ?>" href="staff">staff</a>
                </span>
              </div>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                networks
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item <?php if ($page_content == "networks") {echo "active";} ?>" href="networks">overview</a>
                  <a class="dropdown-item <?php if ($page_content == "consultative") {echo "active";} ?>" href="consultative">consultative panel</a>
                  <a class="dropdown-item" href="#">join the REI networks</a>
                </span>
              </div>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                projects
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item <?php if ($page_content == "projects") {echo "active";} ?>" href="projects">overview</a>
                  <a class="dropdown-item <?php if ($page_content == "applications") {echo "active";} ?>" href="applications">Project Applications</a>
                  <a class="dropdown-item <?php if ($page_content == "projects") {echo "currentprojects";} ?>" href="currentprojects">Current Projects</a>
                  <a class="dropdown-item <?php if ($page_content == "projects") {echo "completedprojects";} ?>" href="completedprojects">Completed Projects</a>
                  <a class="dropdown-item <?php if ($page_content == "projects") {echo "innovationfellows";} ?>" href="innovationfellows">Innovation Fellows</a>
                  <a class="dropdown-item" href="#">Success Stories</a>
                  <a class="dropdown-item" href="#">Comprehensive Economic Recovery Initiative</a>
                  <a class="dropdown-item" href="#">The Tendaji Talks</a>
                </span>
              </div>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                events
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item <?php if ($page_content == "summit2023") {echo "active";} ?>" href="summit2023">Innovate Michigan! Summit 2023</a>
                  <a class="dropdown-item <?php if ($page_content == "events") {echo "active";} ?>" href="events">past events</a>
                </span>
              </div>
            </li>

            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                media
              </a>

              <div class="dropdown-menu">
                <span>
                  <a class="dropdown-item" href="#">E-Updates</a>
                  <a class="dropdown-item" href="#">Videos</a>
                  <a class="dropdown-item" href="#">Podcasts</a>
                  <a class="dropdown-item" href="#">Webinar Series</a>
                  <a class="dropdown-item" href="#">News</a>
                </span>
              </div>
            </li>

            <li>
              <a href="contact">
                contact
              </a>
            </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
