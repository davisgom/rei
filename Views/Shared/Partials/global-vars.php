<?php
$site_title = "University Center for Regional Economic Innovation";

$cced = "Center for Community and Economic Development";

$keywords = "outreach engagement scholar scholarship community development responsive build capacity building collaborative collaboration michigan state university";

$author = "University Outreach and Engagement - Communication and Information Technology";

$description = "University Outreach and Engagement is a campus-wide central resource that assists MSU academic units construct more effective engagement with communities.";

$main_phone = "(517) 353-8977";

$external = 'target="_blank" rel="noopener" data-toggle="tooltip" data-placement="bottom" title="Link opens in new window"';

?>
