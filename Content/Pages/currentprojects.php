<?php
$page_title="Current Projects"; 

$page_title_display="d-none";

$theme_header = "projects";

$theme_header_content =
"
    <p>
    To learn more about REI's various project types, go to our <a href='projects'>Projects Overview</a> page.
    </p>
";
?>





<h2>
    Innovation Fellows <br /> <small>(Co-Implementation Plans)</small>
</h2>

<ul class="projects-list row">
<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                Collaborative Partnership Models: A Case for Increased Capacity and Efficiency
            </a>
        </h2>
        <p>
            Innovation Fellow: <strong>Robert Carson</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam. 
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>

<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                Benefits of Worker-Owned Cooperative Business Development for Low-Income Residents in Michigan
            </a>
        </h2>
        <p>
            Innovation Fellow: <strong>Margo Dalal</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam. 
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>
</ul>


<h2>
    Co-Learning Plans
</h2>

<ul class="projects-list row">
<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                Making Ends Meet: Women's Work, the Care Sector and Regional Informal Economies: Detroit
            </a>
        </h2>
        <p>
            Author: <strong>Dr. Louise Jezierski, Dr. Sejuti Das Gupta</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam.
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>

<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                Leveraging the Planning Process to Create a Model of Engagement for Communities in Need
            </a>
        </h2>
        <p>
            Author: <strong>Dr. John (Jake) Parcell</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam.  
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>

<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                Opportunity or Betrayal?: The Promise and Perils of Electric Mobility
            </a>
        </h2>
        <p>
            Author: <strong>Dr. Mark Wilson, Dr. Zeenat Kotval-Karamchandani, Shane Wilson</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam. 
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>
</ul>

<h2>
    Student-Led, Faculty-Guided <br class="d-none d-lg-block" /> Technical Assistance
</h2>

<ul class="projects-list row">
<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                Crowdfunding Capital Raises
            </a>
        </h2>
        <p>
            SLFG Team:: <strong>Dr. Dirk Colbry</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam. 
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>

<li class="card project-card col-12">
    <div class="details">
        <h2>
            <a href="project">
                MSU Urban and Regional Planning Practicum Course
            </a>
        </h2>
        <p>
            SLFG Team:: <strong>Dr. Zenia Kotval and Community Partners</strong>
        </p>

        <hr />

        <p class="project-summary">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente animi expedita velit nihil, at cumque tempora? Libero optio, ipsa ad voluptatum consequuntur fugiat commodi. Excepturi doloribus voluptatibus perspiciatis dolorum quisquam. 
        </p>

        <a href="project" class="project-link">
            Learn More
        </a>
    </div>
</li>
</ul>