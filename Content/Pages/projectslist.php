<?php
$page_title="Project Topic Title"; 

$page_title_display="d-none";

$theme_header = "projects";

$theme_header_content =
"

<p>
    Id aliquet lectus proin nibh nisl condimentum id. At elementum eu facilisis sed odio morbi quis commodo odio. Iaculis nunc sed augue lacus viverra vitae congue. Maecenas accumsan lacus vel facilisis volutpat est.
</p>

";
?>

<ul class="projects-list-full">

<?php
$fname = array("Will", "Willis", "Willa", "William", "Willemina");

for ($i = 1;$i <= 10; $i++) {
    echo '
    
        <li class="project row">
            <div class="photo col-md-3">
                <a href="project">
                    <img class="img-fluid" src="Content/Images/placeholder-image-3x4.jpg" alt="Photo of ##">
                </a>
            </div>

            <div class="details col-md-8">
                <h2>
                    <a href="project">
                        Project Title
                    </a>
                </h2>
                <p>
                    Authors: <strong>'.$fname[array_rand($fname)].'  Spartan</strong>
                </p>
                
                <p class="project-summary">
                    Sed pulvinar proin gravida hendrerit lectus. Vel orci porta non pulvinar neque laoreet suspendisse interdum. Mattis nunc sed blandit libero. Ut venenatis tellus in metus vulputate. Integer vitae justo eget magna fermentum iaculis eu. Nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Bibendum at varius vel pharetra vel. Enim lobortis scelerisque fermentum dui faucibus. Aliquet nec ullamcorper sit amet risus nullam eget. Sodales neque sodales ut etiam.
                </p>
                
                <a href="#" class="btn btn-theme-small btn-theme btn-theme-secondary">Read the Report</a>
                
                <a href="project" class="btn btn-theme-small btn-theme btn-theme-primary">About the Project</a>
            </div>
        </li>
    
    ';
    }
?>

</ul>