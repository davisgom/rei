<p>
<span class="text-muted">EDA University Center for</span> <br />
<strong class="h4">Regional Economic Innovation</strong> <br />
1615 E. Michigan Ave. <br />
Lansing, MI 48912 <br />
Telephone: 517-353-9555 <br />
Fax: 517-884-6489 <br />
Email: rei@msu.edu
</p>

<section class="cei-social contact-social">
  <div class="container">
    <div class="row">
      <div class="connect-intro col-md-8 my-4">
      <h2 class="w-100">Connect with CEI</h2>
    
        <p>
          We would love the opportunity to connect with you on social media, and share more content with you about innovation, community, and more.
        </p>
      </div>
    
      <div class="social-icons mb-5 ml-0">
          <a href="#" class="btn btn-theme-social facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social youtube"><i class="fab fa-youtube" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social instagram"><i class="fab fa-instagram" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social linkedin"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social newsletter"><i class="fa fa-envelope" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</section>

<p>
  <a href="#">For more detailed contact information, visit the staff directory</a>
</p>