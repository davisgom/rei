<?php
$page_title="2023 Project Applications";

$page_title_display="d-none";

$theme_header = "projects";

$theme_header_content =
'
    <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Totam animi unde sapiente magnam itaque vel soluta obcaecati dolores.        
    </p>
';
?>

<div class="alert alert-danger mb-5">
    <strong class="h3 fw-800">
        Applications for the 2023 Project year are now closed.
    </strong>
</div>

<p>REI funds several types of projects:</p>

<h2><a name="co-learning"></a>Co-Learning Projects</h2>

<p><a href="#">Co-Learning Plan</a> selection is a competitive process. REI typically funds up to four Co-Learning Projects each year, and authors receive up to $8,000 in support. Visit the <a href="projects">Project Overview</a> page for more information on REI project types.&nbsp;</p>

<a class="mb-5 btn btn-theme btn-theme-accent disabled" style="filter:grayscale(100%); opacity:.5;" href="#">Apply Now!</a>

<h3>Co-Learning Plan Timeline</h3>

<table class="table table-bordered">
	<thead class="thead-dark">
		<tr>
			<th scope="col">Stage</th>
			<th scope="col">Date</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">Application Deadline</th>
			<td>December 12, 2022</td>
		</tr>
		<tr>
			<th scope="row">Notice of Awards</th>
			<td>January 2023</td>
		</tr>
		<tr>
			<th scope="row">First Drafts Due</th>
			<td>April 3, 2023</td>
		</tr>
		<tr>
			<th scope="row">Second Drafts Due</th>
			<td>May 5, 2023</td>
		</tr>
		<tr>
			<th scope="row">Executive Summary Due</th>
			<td>June 2, 2023</td>
		</tr>
		<tr>
			<th scope="row">Final Report Due</th>
			<td>July 7, 2023</td>
		</tr>
		<tr>
			<th scope="row">Presentations Due</th>
			<td>August 4, 2023</td>
		</tr>
		<tr>
			<th scope="row">Annual REI Event</th>
			<td>August 17, 2023</td>
		</tr>
		<tr>
			<th scope="row">Addendum Due</th>
			<td>August 31, 2023</td>
		</tr>
	</tbody>
</table>

<h2><a name="innovation_fellows"></a>Innovation Fellows Program</h2>

<p><a href="#">Innovation Fellows</a> are funded through a competitive award process to which only serious, dedicated individuals should apply. Visit the <a href="projects">Project Overview</a> page for more information on REI project types.&nbsp;</p>

<a class="mb-5 btn btn-theme btn-theme-accent disabled" style="filter:grayscale(100%); opacity:.5;" href="#">Apply Now!</a>

<h3>Innovation Fellow Timeline</h3>

<table class="table table-bordered">
	<thead class="thead-dark">
		<tr>
			<th scope="col">Stage</th>
			<th scope="col">Date</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">Applications Due</th>
			<td>December 12, 2022</td>
		</tr>
		<tr>
			<th scope="row">Notice of Award</th>
			<td>January, 2023</td>
		</tr>
		<tr>
			<th scope="row">First Project Update</th>
			<td>April 3, 2023</td>
		</tr>
		<tr>
			<th scope="row">Second Project Update</th>
			<td>May 5, 2023</td>
		</tr>
		<tr>
			<th scope="row">Third Project Update</th>
			<td>June 2, 2023</td>
		</tr>
		<tr>
			<th scope="row">Fourth Project Update</th>
			<td>July 7, 2023</td>
		</tr>
		<tr>
			<th scope="row">Final Report Due</th>
			<td>August 25, 2023</td>
		</tr>
		<tr>
			<th scope="row">Present at REI Event</th>
			<td>August 17, 2023</td>
		</tr>
	</tbody>
</table>

<h2><a name="student-led"></a>Student-Led, Faculty-Guided Technical Assistance</h2>

<p>Technical Assistance selection is a competitive process. REI typically funds five&nbsp;or more semester-long projects each year. Visit the <a href="projects">Project Overview</a> page for more information on REI project types, or <a href="#">watch our Webinar</a> on the 2019-2020 Student-Led, Faculty-Guided technical assistance.&nbsp;</p>

<a class="mb-5 btn btn-theme btn-theme-accent disabled" style="filter:grayscale(100%); opacity:.5;" href="#">Apply Now!</a>

<h3>Technical Assistance Timeline (Flexible)</h3>

<table class="table table-bordered">
	<thead class="thead-dark">
		<tr>
			<th scope="col">Stage</th>
			<th scope="col">Fall</th>
			<th scope="col">Spring</th>
			<th scope="col">Summer</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">Application Opens</th>
			<td>June</td>
			<td>October</td>
			<td>February</td>
		</tr>
		<tr>
			<th scope="row">Applications Due</th>
			<td>July</td>
			<td>November</td>
			<td>March</td>
		</tr>
		<tr>
			<th scope="row">Applicant Selection</th>
			<td>August</td>
			<td>December</td>
			<td>April</td>
		</tr>
		<tr>
			<th scope="row">Final Product Due</th>
			<td>December</td>
			<td>April</td>
			<td>August</td>
		</tr>
	</tbody>
</table>