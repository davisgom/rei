<?php
$page_title="Project Title"; 

$page_title_display="d-none";

$theme_header = "projects";

$theme_header_content =
"

<hr />
<p>
  Creativity and Invention
</p>

";
?>

<ul class="list-unstyled mt-n5">
    <li>
        <span class="text-muted">Authors:</span> <strong> Willa Spartan</strong>
    </li>

    <li>
        <span class="text-muted">Project Year:</span> 20<?php echo random_int(12,23); ?>
    </li>

    <li>
        <span class="text-muted">Project Type:</span> Co-Learning Plan
    </li>
</ul>


<h2>
    Summary
</h2>

<p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada. Aliquam ultrices sagittis orci a scelerisque. Feugiat nisl pretium fusce id velit ut tortor pretium. Orci phasellus egestas tellus rutrum tellus pellentesque eu tincidunt. Adipiscing elit pellentesque habitant morbi tristique senectus. Nunc congue nisi vitae suscipit tellus. Eget dolor morbi non arcu risus quis varius quam. Senectus et netus et malesuada fames ac turpis. Integer quis auctor elit sed. Nulla facilisi cras fermentum odio. Massa sapien faucibus et molestie ac. Rhoncus urna neque viverra justo nec ultrices dui sapien. Cras pulvinar mattis nunc sed blandit libero volutpat. Vitae tempus quam pellentesque nec nam aliquam sem.
</p>

<p>
    Id aliquet lectus proin nibh nisl condimentum id. At elementum eu facilisis sed odio morbi quis commodo odio. Iaculis nunc sed augue lacus viverra vitae congue. Maecenas accumsan lacus vel facilisis volutpat est. Ut morbi tincidunt augue interdum velit euismod in pellentesque. Turpis egestas pretium aenean pharetra. Mattis vulputate enim nulla aliquet porttitor lacus. Sapien pellentesque habitant morbi tristique senectus et netus et. Sodales ut etiam sit amet nisl. Mauris a diam maecenas sed enim ut sem viverra aliquet.
</p>

<p>
    Sed pulvinar proin gravida hendrerit lectus. Vel orci porta non pulvinar neque laoreet suspendisse interdum. Mattis nunc sed blandit libero. Ut venenatis tellus in metus vulputate. Integer vitae justo eget magna fermentum iaculis eu. Nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Bibendum at varius vel pharetra vel. Enim lobortis scelerisque fermentum dui faucibus. Aliquet nec ullamcorper sit amet risus nullam eget. Sodales neque sodales ut etiam.
</p>

<p>
    Viverra tellus in hac habitasse. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam. Urna porttitor rhoncus dolor purus non. Sed vulputate mi sit amet mauris commodo quis imperdiet. Tellus elementum sagittis vitae et leo. Pharetra vel turpis nunc eget lorem. Eget felis eget nunc lobortis mattis aliquam. Rhoncus mattis rhoncus urna neque viverra. Non tellus orci ac auctor. Luctus accumsan tortor posuere ac ut consequat semper viverra nam. Tellus orci ac auctor augue mauris augue neque. Imperdiet nulla malesuada pellentesque elit eget.
</p>


<h2>
    Author Information
</h2>

<div class="row">
    <div class="col-12 col-md-3 mb-4">
        <img class="img-fluid person-photo mb-2" src="Content/Images/pet-portrait-<?php echo random_int(1,12); ?>.jpg" alt="Photo of ##">

        <p>
            <strong> Willa Spartan</strong> <br />
            <span>Author Title</span>
        </p>
    </div>

    <div class="col-12 col-md-9">
        <p>
            Aliquam ut porttitor leo a. Ut enim blandit volutpat maecenas volutpat. Maecenas accumsan lacus vel facilisis volutpat est velit. Nam libero justo laoreet sit amet cursus sit. Phasellus vestibulum lorem sed risus ultricies tristique. Quam quisque id diam vel quam elementum. Mollis nunc sed id semper risus in hendrerit gravida rutrum. Amet justo donec enim diam vulputate. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Pharetra magna ac placerat vestibulum lectus mauris. Lacus luctus accumsan tortor posuere ac ut consequat semper. Quis blandit turpis cursus in hac habitasse platea dictumst. Nibh tellus molestie nunc non blandit massa enim nec dui. Sed ullamcorper morbi tincidunt ornare. Habitasse platea dictumst quisque sagittis purus sit amet volutpat. Viverra tellus in hac habitasse. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam. Urna porttitor rhoncus dolor purus non. Sed vulputate mi sit amet mauris commodo quis imperdiet.
        </p>
    </div>
</div>

<hr class="divider" />

<a href="#" class="btn btn-theme btn-theme-primary">
    View project report
</a>