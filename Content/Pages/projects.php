<?php
$page_title="2023 Project Year"; 

$page_title_display="d-none";

$theme_header = "projects";

$theme_header_content =
"
    <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Totam animi unde sapiente magnam itaque vel soluta obcaecati dolores.        
    </p>
";


?>





<p>
    Welcome to the 2023 project year! The MSU U.S. EDA University Center for Regional Economic Innovation (REI) funds several types of projects: <a href="#">Co-Learning Projects</a>, researched and written by professionals; <a href="#">Co-Implementation Plans</a>, led by Innovation Fellows; and <a href="#">Student-Led, Faculty-Guided Technical Assistance</a> projects, researched and written by student teams at Michigan universities and colleges. Please visit the <a href="completedprojects">Completed Projects</a> page for examples of these types of work. You may also view the <a href="applications">Submit an Application</a> page for project timelines and to apply for REI funding. Submitted projects will receive priority if they serve Asset Limited-Income Constrained-Employed (ALICE) populations living within Redevelopment Ready Communities (RRCs) and/or Opportunity Zones (OZs). Projects are encouraged to focus on one of four foci areas, including: resiliency planning, financial resiliency, circular economies, or 21st century communications infrastructure.
</p>

<p>
    Within these foci, please select 1 of the 6 topical areas for the 2023 project year.
</p>

<ol>
    <li>    
        <p>
            <strong>Clean Energy Economies of Michigan</strong>: How can cities or rural areas of Michigan make this happen?
        </p>
    </li>

    <li>
        <p>
            <strong>Equitable Economic Development: </strong>Examining new or prior models and/or effective practices in Michigan with discussion of racial policy and practice impacts.
        </p>
    </li>

    <li>
        <p>
            <strong>Circular Economies of Michigan</strong>: What is a circular economy and how might it be catalyzed, incentivized, or accelerated? Are there specific business sectors in Michigan that are strategic for advancing circularity, if yes what can economic developers do to assist in this economic transformation?
        </p>
    </li>

    <li>
        <p>
            <strong>Blight and Deconstruction within Michigan</strong>: What can economic developers do to minimize structural blight and abandonment and support the growth and development of a structural material deconstruction economy in their region?
        </p>
    </li>

    <li>   
        <p>
            <strong>Michigan Planning Organizations Leveraging the Planning Process Together for Greater Impact</strong>: What tools, models, policies and programs can be adopted to advance regional planning in Michigan? How can the planning process be leveraged, including by working together, to help build capacity for overstretched communities?
        </p>
    </li>

    <li>
        <p><strong>Other</strong></p>
    </li>
</ol>

<br />

<p class="alert alert-warning">
    Not sure if your project fits? Please send questions to Jenan Jondy at jondyjen@msu.edu. 
</p>

<br />

<a class="btn btn-theme btn-theme-accent" href="applications">Apply Now!</a>