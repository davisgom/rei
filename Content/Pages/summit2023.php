<?php
    $page_title="Innovate Michigan! Summit 2023";

    $page_title_display="d-none";
?>

<img src="Content/Images/summitbanner_2023.jpg" alt="Innovate Michigan! Summit - August 17th, 2023" class="img-fluid"/>

<h2>
    Save the date for the <br /> 2023 Innovate Michigan! Summit
</h2>

<p>
    The 12th annual <em>Innovate Michigan! Summit</em> will be on <strong>Thursday, August 17th, 2023</strong> at the Kellogg Hotel & Conference Center in East Lansing, Michigan. The <em>Innovate Michigan! Summit</em> features presentations on innovative economic development tools, models, programs, and policies from around the state.
</p>

<p>
    View the 2023 Project Series that will be presented at the summit, on our <a href="currentprojects">current projects</a> page.
</p>

<hr class="divider" />

<h2>
    Our Co-Host, the Michigan's Inventor's Coalition (MIC)
</h2>
<div class="d-sm-flex align-items-end">
    <p>
        The 11th Annual Michigan Inventors Expo, happening on August 17th, brings together problem solvers, inventors, and entrepreneurs for a day full of networking, pitches, and education
    </p>

    <img src="Content/Images/mic-logo.png" width="200" alt="Michigan Inventors Coalition"/>
</div>



<hr class="divider" />

<div class="alert alert-warning">
<h2>
    Ideas to Innovation
</h2>

<p class="lead">
    Help the MSU EDA University Center for Regional Economic Innovation develop equitable economic development project ideas for 2024!
</p>

<hr />

<p>
    Have an idea for an equitable economic development project? Your innovative idea submission could be broad or focused and also anonymous if you choose. <strong>Your topical project ideas will inform our 2024 call for proposals that are most likely to create or retain jobs and/or businesses in economically distressed communities and regions of Michigan.</strong> You may submit as many topical ideas for projects as desired. Thank you for your thoughts and keep them coming!
</p>

<p>
    <a href="https://msu.co1.qualtrics.com/jfe/form/SV_agGj2Agh23wMW90" target="_blank" class="btn btn-theme btn-theme-primary">
        Submit an Innovation Idea
    </a>
</p>
</div>

<hr class="divider" />

<p>Suppoterd by:</p>
<img src="Content/Images/eda-logo.svg" width="280" alt="U.S. Economic Development Administration" />