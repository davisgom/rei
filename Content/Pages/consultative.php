<?php $page_title="Consultative Panel" ?>

<p>
    At the hub of the REI University Center is the Consultative Panel of statewide knowledge-based and experienced experts. The purpose of this select panel is to counsel and advise the REI University Center faculty and staff on the overall project objectives and scope of work.
</p>

<ul class="people-list">

<?php
$fname = array("Will", "Willis", "Willa", "William", "Willemina");
$tname = array("Project Coordinator", "Research Assistant", "Senior Research Assistant");
$dept = array("Center for Community and Economic Development", "Regional Economic Innovation");

for ($i = 1;$i <= 31; $i++) {
    echo '
    
        <li class="card person-card">
            <p class="photo">
                <a href="person">
                    <img class="img-fluid img-thumbnail project-team" src="Content/Images/placeholder-image-4x3.jpg" alt="Photo of ##">
                </a>
            </p>

            <div class="details-blackbg"></div>
            
            <div class="details">
                <h2>
                    <a href="person">
                    '.$fname[array_rand($fname)].'  Spartan
                    </a>
                </h2>
                <p><strong>'.$tname[array_rand($tname)].'</strong>, <br /> '.$dept[array_rand($dept)].'</p>        
            </div>
        </li>
    
    ';
    }
?>

</ul>