<div class="row">
    <div class="col-md-7">
        <hr class="divider" />
    </div>    
    
    <div class="col-12 col-md-5 order-md-2 mb-4">
        <img class="img-fluid person-photo" src="Content/Images/pet-portrait-<?php echo random_int(1,12); ?>.jpg" alt="Photo of ##">
    </div>

    <div class="col-12 col-md-7 order-md-1">
        <p class="lead">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam eligendi dignissimos maxime voluptatum in eaque omnis quas optio ipsa iusto.
        </p>

        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tortor at auctor urna nunc id. Eget dolor morbi non arcu risus quis varius quam. Dictum sit amet justo donec enim diam vulputate. Fringilla est ullamcorper eget nulla facilisi. Tortor at auctor urna nunc id cursus metus aliquam eleifend. Vitae semper quis lectus nulla. Quisque sagittis purus sit amet volutpat consequat mauris. Vivamus arcu felis bibendum ut tristique et egestas. Massa sed elementum tempus egestas sed sed risus. Amet consectetur adipiscing elit duis tristique sollicitudin nibh. Nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Eget nulla facilisi etiam dignissim diam quis.
        </p>

        <p>
            Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam. Libero enim sed faucibus turpis in. Amet justo donec enim diam. Volutpat sed cras ornare arcu dui vivamus arcu felis. Nec dui nunc mattis enim ut tellus. Magna etiam tempor orci eu lobortis elementum nibh tellus molestie. Facilisis mauris sit amet massa vitae tortor. Pretium nibh ipsum consequat nisl vel pretium lectus quam id. Dolor sit amet consectetur adipiscing elit duis tristique sollicitudin. Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Cursus euismod quis viverra nibh cras pulvinar. Sit amet massa vitae tortor condimentum lacinia. Euismod quis viverra nibh cras pulvinar mattis nunc sed. Pretium vulputate sapien nec sagittis. Vel risus commodo viverra maecenas accumsan. Cursus metus aliquam eleifend mi. Dictum sit amet justo donec enim diam. Vitae semper quis lectus nulla at volutpat diam ut.
        </p>

        <p>
            In dictum non consectetur a. Eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Ac placerat vestibulum lectus mauris ultrices eros in cursus. Viverra mauris in aliquam sem. Enim ut sem viverra aliquet eget sit. Elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Risus in hendrerit gravida rutrum quisque non. Purus sit amet volutpat consequat. Nibh sed pulvinar proin gravida. Leo vel fringilla est ullamcorper eget. Ac auctor augue mauris augue neque gravida. Nullam eget felis eget nunc lobortis mattis aliquam. Rhoncus urna neque viverra justo nec. Posuere ac ut consequat semper viverra nam libero justo laoreet. Mauris sit amet massa vitae. Morbi tincidunt ornare massa eget egestas purus viverra accumsan in. Lectus arcu bibendum at varius vel pharetra vel. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et.
        </p>
    </div>
</div>