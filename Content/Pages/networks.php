<p>
    The Regional Economic Innovation (REI) Network is a diverse collaboration of individuals and organizations that exists to promote innovation within Michigan's regional economic ecosystems. The REI Network is a community of over 1,000 individuals from the public and private sectors who participate in a facilitation and solicitation process to identify, create, and promote innovations in research, science, technology, and skill training to catalyze growth in their regional economies. Below are the categorical areas that make up the REI Network.
</p>


<ul class="network-list">
	<li>
        <strong>Regional Talent:</strong>
        To identify innovations that have the potential to accelerate growth in existing regional economies, particularly with respect to innovations in skill training and workforce development.
    </li>

	<li>
        <strong>Discover to Market:</strong>
        To assist researchers, inventors and scientists in transitioning their discoveries into marketable goods and services.
    </li>
	
    <li>
        <strong>Innovation Infrastructure:</strong>
        To support economic innovation by connecting enterprises with the necessary physical and digital infrastructure needed to sustain and expand a highly advanced economy.
    </li>
	
    <li>
        <strong>Growth and Equity:</strong>
        To ensure that during the development process, particular attention is paid to the needs and perspectives of Michigan's most distressed regional economic ecosystems in order to promote equitable statewide economic development.
    </li>
	
    <li>
        <strong>MSU Coordination and the Higher Education and Research:</strong>
        To connect and coordinate the resources and assets of Michigan's universities and colleges to best assist Michigan's high-growth entrepreneurial regional and local economic ecosystems, and the work of the MSU EDA REI University Center specifically.
    </li>
</ul>

<p>
    If you would like to join the REI Network and become a part of the Regional Economic Innovation ecosystem in Michigan, please fill out the <a href="#s">sign up form</a>.
</p>


<h2>
    Network Map
</h2>

<?php
    $map_link="
    https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d11863651.195602097!2d-97.15701539644245!3d35.24086779122482!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1683165437809!5m2!1sen!2sus
    ";
?>


<section class="map" id="'.$page_content.'">
    <div class="embed-responsive embed-responsive-16by9" style="min-height: 400px;">
        <iframe src="<?php echo $map_link; ?>"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
</section>


<section class="cei-social contact-social">
  <div class="container">
    <div class="row">
      <div class="connect-intro col-md-8">
        <h2 class="w-100">Connect with CEI</h2>
    
        <p>
          We would love the opportunity to connect with you on social media, and share more content with you about innovation, community, and more.
        </p>
      </div>
    
      <div class="social-icons mb-5 ml-0">
          <a href="#" class="btn btn-theme-social facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social youtube"><i class="fab fa-youtube" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social instagram"><i class="fab fa-instagram" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social linkedin"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
    
          <a href="#" class="btn btn-theme-social newsletter"><i class="fa fa-envelope" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</section>