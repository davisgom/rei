<section class="homepage-intro  text-lg-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-8">
        
        <p>
        Discovering and applying new and innovative economic development tools, models, policies, and programs<br />
        <span class="fs-70 fw-400">Creating Jobs and Wealth in Distressed Michigan Communities </span>
        </p>

        <a href="about" class="btn btn-theme btn-theme-accent">
          Learn More
        </a>
      </div>
    </div>
  </div>
</section>

<section class="alert-banner" id="alert-banner" onclick="swapAlert()">
  <div class="container">
    <div class="row">
      <div class="col-12 d-flex justify-content-center py-2">
        <p class="mb-0">
          <i class='fas fa-exclamation-circle'></i> <span id="appAlert">Applications for the 2023 project year are now closed</span>
        </p>
      </div>
    </div>
  </div>
</section>

<script>
function swapAlert() {
  var x = document.getElementById("appAlert");
  if (x.innerHTML === "Applications for the 2023 project year are now closed") {
    x.innerHTML = "Applications for the 2023 project year are now open";
  } else {
    x.innerHTML = "Applications for the 2023 project year are now closed";
  }

  var element = document.getElementById("alert-banner");
   element.classList.toggle("open");
}

</script>

<!-- **** FEATURE AND NEWS SECTION **** -->
<section class="feature-and-news">
  <div class="container">
    <div class="row">
      <div class="feature col-12 col-lg-6 col-xl-8">
        
        <div class="container">

          <!-- **** EXAMPLE FOR FEATURED TEXT ITEM **** -->
          <div class="row">
            <div class="col-xl-4 mb-3">
              <img src="Content/Images/lightbulb.jpg" class="img-fluid" />
            </div>

            <div class="col-12 col-xl-8">
              <h2>Ideas for Innovation</h2>
              <p>
              In order to prepare for the upcoming year, REI needs your help to decide what topics to solicit Co-Learning Plan authors for.
              </p>
              <a href="#" class="btn btn-theme btn-theme-tertiary">Take Survey</a>
            </div>
          </div>

        </div>

      </div>
      
      <div class="news col-12 col-lg-6 col-xl-4">
        
        <div class="container">
          <div class="row">
            <div class="col">
              <h2>News</h2>

              <!-- **** IMPORTANT: FOR DESIGN PLEASE ONLY DISPLAY THREE ITEMS!! **** -->

              <article>
                <h3><a href="#">
                  MSU EDA University Center Work Highlighted in EDA Newsletter
                </a></h3>
              </article>
              
              <article>
                <h3><a href="#">
                  SLFG Projects Presented at the 2023 Planning Practicum Team Presentations
                  </a></h3>
              </article>

              <article>
                <h3><a href="#">
                  Policy Drives Change: MEDA Capitol Day
                </a></h3>
              </article>


              <br />

              <a href="#" class="btn btn-sm btn-outline-secondary">
                View all
              </a>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div> 
</section>


<!-- **** SOCIAL MEDIA BUTTONS SECTION **** -->

<?php include("Views/Shared/Partials/social.php"); ?>

<!-- **** LINKS TO OTHER SITES **** -->

<section class="ced-sites">

  <div class="container">    
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-center">    
          
          <a href="#" class="text-hide site cced-logo-color">
            <?php echo $cced; ?>
          </a>
  
          <span class="d-block"></span>
  
          <a href="https://cei.mdavis.in" class="text-hide site cei-logo-color">
            Circular Economy Institute
          </a>
  
          <span class="d-block"></span>
  
          <a href="#" class="text-hide site domicology-logo-color">
            Domicology
          </a>
  
        </div>
      </div>
    </div>
  </section>

<!-- **** PROJECTS SECTION **** -->

<section class="projects">
  <div class="container">
    <div class="row">
      <h2>Projects</h2>

  <div class="row">
      <div class="col-md-12">
      
              <ul>
                  <li class="item">
                      <span>
                       <h3> Co-Implementation Plans </h3>                      
                      <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam ad consequatur officiis excepturi dolor facere odio reprehenderit, a pariatur totam repellat voluptatem repudiandae, nostrum soluta quae nobis autem natus eaque.
                      </p>
                      <a class="btn btn-theme btn-theme-secondary" href="#">Learn More</a>
                      </span>
                  </li>
                  
                  <li class="item">
                      <span>
                    <h3> Co-Learning Plans </h3>
                      <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam ad consequatur officiis excepturi dolor facere odio reprehenderit, a pariatur totam repellat voluptatem repudiandae, nostrum soluta quae nobis autem natus eaque.
                      </p>
                      <a class="btn btn-theme btn-theme-secondary" href="#">Learn More</a>
                      </span>
                  </li>
                  
                  <li class="item">
                      <span> 
                      <h3> Student-Led/ Faculty-Guided </h3>
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam ad consequatur officiis excepturi dolor facere odio reprehenderit, a pariatur totam repellat voluptatem repudiandae, nostrum soluta quae nobis autem natus eaque.
                      </p>
                      <a class="btn btn-theme btn-theme-secondary" href="#">Learn More</a>
                      </span>
                  </li>
              </ul>

      </div>
  </div>
    </div>
  </div>  
</section>

<!-- **** PARTNER LOGOS **** -->

<section class="partner-logos">
  <div class="container">
    <div class="row">

      <div class="d-none d-sm-block col-sm-1"> <!-- SPACER DIV --></div>
    
      <div class="col-6 col-sm-2">
        
          <img src="Content/Images/eda-logo.svg" class="img-fluid" alt="U.S. Economic Development Administration" />
        
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://research.msu.edu/" target="_blank">
          <img src="Content/Images/msu-ori-logo.svg" class="img-fluid" alt="MSU Vice President of Research and Innovation" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://provost.msu.edu/" target="_blank">
          <img src="Content/Images/msu-provost-logo.svg" class="img-fluid" alt="MSU Office of the Provost" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://engage.msu.edu/" target="_blank">
          <img src="Content/Images/msu-uoe-logo.svg" class="img-fluid" alt="MSU University Outreach and Engagement" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://www.canr.msu.edu/outreach/" target="_blank">
          <img src="Content/Images/msu-extension-logo.svg" class="img-fluid" alt="MSU Extension" />
        </a>
      </div>
      
      <div class="d-none d-sm-block col-sm-1"> <!-- SPACER DIV --></div>

      <div class="d-none d-sm-block col-sm-1"> <!-- SPACER DIV --></div>

      <div class="col-6 col-sm-2">
        <a href="https://comartsci.msu.edu/" target="_blank">
          <img src="Content/Images/msu-commarts-logo.svg" class="img-fluid" alt="MSU College of Communication Arts and Sciences" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://jmc.msu.edu/" target="_blank">
          <img src="Content/Images/msu-jmc-logo.svg" class="img-fluid" alt="MSU James Madison College" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://broad.msu.edu/supply-chain-management/" target="_blank">
          <img src="Content/Images/msu-dscm-logo.svg" class="img-fluid" alt="MSU Department of Supply Chain Management" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://broad.msu.edu/burgess-institute/" target="_blank">
          <img src="Content/Images/msu-biei-logo.svg" class="img-fluid" alt="MSU Burgess Institute for Entrepreneurship and Innovation" />
        </a>
      </div>

      <div class="col-6 col-sm-2">
        <a href="https://quello.msu.edu/" target="_blank">
          <img src="Content/Images/msu-qc-logo.svg" class="img-fluid" alt="MSU Quello Center" />
        </a>
      </div>
          
    </div>
  </div>
</section>