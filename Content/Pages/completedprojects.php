<?php
$page_title="Completed Projects"; 

$page_title_display="d-none";

$theme_header = "projects";

$theme_header_content =
"
    <p>
        A selection of REI project categories is listed here.
    </p>
";


?>

<section class="topics">
    <a href="projectslist" class="topic abandonment">
    <div class="col-11">
        <h2>Abandonment, Revitalization, and Development</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic creativity">
    <div class="col-11">
        <h2>Creativity and Invention</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic digital">
    <div class="col-11">
        <h2>Digital</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic entrepreneurship">
    <div class="col-11">
        <h2>Entrepreneurship</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic finance">
    <div class="col-11">
        <h2>Finance, Legal, and Exporting</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic green">
    <div class="col-11">
        <h2>Green and Food</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View All Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic local">
    <div class="col-11">
        <h2>Local Technical Assistance</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>

    <a href="projectslist" class="topic workforce">
    <div class="col-11">
        <h2>Workforce and Education</h2>

        <i class="fas fa-2x fa-arrow-circle-right">
        <span>View Projects</span>
        </i>
    </div>
    </a>
</section>