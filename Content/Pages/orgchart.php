<?php $page_title="Organizational Chart" ?>

<br />

<ul class="org-chart">
    <li>
        <span><strong>Michigan Sate University</strong></span> <div class="arrow"></div>
    </li>

    <li>
        <span><strong>University Outreach and Engagement</strong></span> <div class="arrow"></div>
    </li>

    <li>
        <span><strong><?php echo $cced; ?></strong> (CCED)</span>

        <ul class="level-2">
            <li>
                <div class="arrow"></div>
                <span><strong>Environment, Great Lakes, <br /> and Engergy</strong> (EGLE)</span>
            </li>

            <li>
                <div class="arrow"></div>
                <span><strong>Comprehensive Economic <br /> Recovery Initiative</strong> (CERI)</span>
            </li>

            <li>
                <div class="arrow"></div>
                <span><strong>Regional Economic <br /> Innovation</strong></span>
            </li>
            
            <li>
                <div class="arrow"></div>
                <span><strong>Domicology</strong></span>
            </li>

            <li>
                <div class="arrow"></div>
                <span><strong>CCED Flint</strong></span>
            </li>

        </ul>
    </li>
</ul>

<br />
<br />


<h2>MSU CCED</h2>

<p>
    Michigan State University is a land-grant university that has successfully operated applied-research and technical assistance programs for over 150 years. The MSU Center for Community and Economic Development (CCED), located within the Office of the Provost, has worked with communities and regions to enhance economic development for over 45 years. Additionally, CCED and the U.S. Economic Development Administration (EDA) have maintained a mutually beneficial relationship since 1987.
</p>