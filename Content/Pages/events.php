<?php
    $page_title = "Innovate Michigan! Summit"
?>

<p>
    Join economic development practitioners, business leaders, finance providers, scholars, students, local and state government, and other community leaders for a full-day event featuring presentations on innovative economic development tools, models, policies, practices, student technical assistance projects and Michigan inventions. Learn, discuss, influence, and share new approaches to economic development in Michigan.
</p>

<h2>Past Summits</h2>

<li><a href="#">Michigan! Summit 2022</a>
<li><a href="#">Michigan! Summit 2021</a>
<li><a href="#">Michigan! Summit 2020</a>
<li><a href="#">Michigan! Summit 2019</a>
<li><a href="#">Michigan! Summit 2018</a>
<li><a href="#">Michigan! Summit 2017</a>
<li><a href="#">Michigan! Summit 2016</a>
<li><a href="#">Michigan! Summit 2015</a>
<li><a href="#">Michigan! Summit 2014</a>
<li><a href="#">Michigan! Summit 2013</a>
<li><a href="#">Michigan! Summit 2012</a>