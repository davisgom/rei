<?php $page_title="Innovation Fellows" ?>

<p>
    The REI Innovation Fellows program was established in 2016 to tackle the tough economic development issues that our distressed communities of Michigan face every day by supporting Innovation Fellows. Innovation Fellows provide on-the-ground support and coordination to turn concepts into actions, and implement new economic development tools, models, and policies. Innovation Fellows, recruited by REI, its partners, Economic Development Districts (EDDs), and past Co-Learning Plan authors, will identify recommendations in past Co-Learning Plans to serve as current and practical information for local and state economic development practitioners and policymakers as they consider important decisions for Michigan communities and regions.
</p>

<ul class="people-list">

<?php
$fname = array("Will", "Willis", "Willa", "William", "Willemina");
$tname = array("Project Coordinator", "Research Assistant", "Senior Research Assistant");
$dept = array("Center for Community and Economic Development", "Regional Economic Innovation");

for ($i = 1;$i <= 13; $i++) {
    echo '
    
        <li class="card person-card">
            <p class="photo">
                <a href="person">
                    <img class="img-fluid img-thumbnail project-team" src="Content/Images/placeholder-image-4x3.jpg" alt="Photo of ##">
                </a>
            </p>

            <div class="details-blackbg"></div>
            
            <div class="details">
                <h2>
                    <a href="person">
                    '.$fname[array_rand($fname)].'  Spartan
                    </a>
                </h2>
                <p><strong>'.$tname[array_rand($tname)].'</strong>, <br /> '.$dept[array_rand($dept)].'</p>        
            </div>
        </li>
    
    ';
    }
?>

</ul>