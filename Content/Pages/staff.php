<ul class="people-list">

<?php
$fname = array("Will", "Willis", "Willa", "William", "Willemina");
$tname = array("Project Coordinator", "Research Assistant", "Senior Research Assistant");
$dept = array("Center for Community and Economic Development", "Regional Economic Innovation");
$photo = range(1,12);

for ($i = 1;$i <= 10; $i++) {
    echo '
    
        <li class="card person-card">
            <p class="photo">
                <a href="person">
                    <img class="img-fluid img-thumbnail project-team" src="Content/Images/pet-portrait-'.$photo[shuffle($photo)].'.jpg" alt="Photo of ##">
                </a>
            </p>

            <div class="details-blackbg"></div>
            
            <div class="details">
                <h2>
                    <a href="person">
                    '.$fname[array_rand($fname)].'  Spartan
                    </a>
                </h2>
                <p><strong>'.$tname[array_rand($tname)].'</strong>, <br /> '.$dept[array_rand($dept)].'</p>        
            </div>
        </li>
    
    ';
    }
?>

</ul>